/* 
    TASK 1

    Необходимо создать объект в котором вы опишите самого себя в формате : "свойство: значение"

    Список свойств:

    - Имя(string)

    - Фамилия(string)

    - Возраст(number)

    - Любимые фильмы(array)

    - Есть ли домашние животные(boolean)

*/

const me = {
    firstName: 'Dmytro',
    lastName: 'Soroka',
    age: 19,
    favoriteFilms: ['Focus', 'Agent 007', '1+1'],
    havePets: true
}

/* 
    TASK 2

    Создайте объект. Пускай это будет объект c описанием вашего любимого музыкального исполнителя.

    Внутри объекта опишите этого испольнителя

    (Имя, пол, возраст, рост,список самых популярных песен),

    и создайте внутри объекта метод.

    Этот метод должен выводить в консоль всю информацию об этом артисте.

    Каждое свойство должно выводиться на отдельной строке.

*/

const favoriteSinger = {
    name: 'Kurt Cobein',
    gender: 'men',
    age: 37,
    topSings: 'Smells Like Teen Spirit, Come As You Are...',
    info: function () {
        for(let key in this) {
            if(typeof this[key] !== 'function') {
                console.log(key + ' : ' + this[key])
            }
        }
    }
}

favoriteSinger.info();

/* 
    TASK 3

    Есть объект:

    const sportsman = {

      Andrew: 'бегун',

      Olga: 'футболист',

      Ivan: 'теннисист',

      Sergey: 'боксер',

      Marina: 'хоккеист'

    };

    Сделайте Ивана фехтовальщиком, не меняя объект руками 
    
*/

const sportsman = {
    Andrew: 'бегун',
    Olga: 'футболист',
    Ivan: 'теннисист',
    Sergey: 'боксер',
    Marina: 'хоккеист'

};

sportsman.Ivan = 'фехтовальщик';

/* 
    TASK 4

    Есть объект:

    const colorCollection = {

      mainPage: {

        header: 'blue',

        footer: 'orange'

      },

      contacts: {

        sidebar: 'white',

        section: 'black'

      },

    };

    Выведите цвет header со страницы mainPage, и sidebar со страницы Contacts в консоль 
    
*/

const colorCollection = {
    mainPage: {
        header: 'blue',
        footer: 'orange'
    },
    contacts: {
        sidebar: 'white',
        section: 'black'
    },
};

console.log(colorCollection.mainPage.header);
console.log(colorCollection.contacts.sidebar);

/* 
    TASK 5

    const staff = {

      'Ivan' : 2000,

      'Andrey' : 1440,

      'Marina' : 5187,

      'Oleg' : 3309,

      'Anna' : 2304,

    };

    Дан объект. Напишите функцию, которая принимает объект с набором сотрудников и возвращает имя самого высокооплачиваемого работника.

    Объект может быть разным, функция должна быть универсальна для любого объекта

*/

const staff = {
    'Ivan': 2000,
    'Andrey': 1440,
    'Marina': 5187,
    'Oleg': 3309,
    'Anna': 2304,
};

function getTopSalary(obj) {
    let topHuman = {
        salary: 0,
        name: ''
    };

    for (let key in obj) {
        if (obj[key] > topHuman.salary) {
            topHuman.salary = obj[key];
            topHuman.name = key;
        }
    };
    return topHuman.name;
}

console.log(getTopSalary(staff));

// Задачи со звездочкой( Опционально ):


/* 
    TASK 6

    const staffWithGender = {

      'Ivan' : {salary: 2000, gender: 'male'},

      'Andrey' : {salary: 1440, gender: 'male'},

      'Marina' : {salary: 5187, gender: 'female'},

      'Oleg' : {salary: 3309, gender: 'male'},

      'Anna' : {salary: 2304, gender: 'female'},

    };

    Необходимо написать МЕТОД , который будет находить женщин

    и в качестве премии прибавит им 10 процентов к зарплате.

    В итоге исходный объект и его свойства должны измениться с учетом премий.

*/

const staffWithGender = {
    'Ivan': {
        salary: 2000,
        gender: 'male'
    },
    'Andrey': {
        salary: 1440,
        gender: 'male'
    },
    'Marina': {
        salary: 5187,
        gender: 'female'
    },
    'Oleg': {
        salary: 3309,
        gender: 'male'
    },
    'Anna': {
        salary: 2304,
        gender: 'female'
    },
    getPremium : function() {
        for(let key in this) {
            if(this[key].gender == 'female') {
                this[key].salary *= 1.1;
            }
        }
    }
};

staffWithGender.getPremium();

console.log(staffWithGender);